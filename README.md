## Introduction

Chat is an client-server app writen on `Java`. It it used to make simple chat communications between different people.

## Tecnical features

* App written on Java 7, using JSON;
* Good performance and resiliency: every user runs in a different thread that provides safe and fast work;
* Easy to deploy and distribute.
* MySQL server required.
* Free in use.
* Available on Linux, Mac OSX and Windows

## Downloads

* [Server](https://bitbucket.org/galinchik2010/chat/downloads/Server.rar)
* [Client](https://bitbucket.org/galinchik2010/chat/downloads/Client.rar)

## Server configuration

### Configuring server

Unzip `Server` archive to desired location.
Open `config.bat` if you are on Windows or run command `java -jar config.jar` in you are on Linux or OS X.
Here is the example how to fulfill server configuration:
```
Enter server port from 1024 to 65535:
1234
Enter server listen backlog:
50
Enter server ip address:
localhost
Enter MySQL database server url:
localhost
Enter MySQL database server DB name:
UsersData
Enter MySQL database server username:
root
Enter MySQL database server password:
""
```

Where:

* `Enter server port from 1024 to 65535` - server port on which it will be started;
* `Enter server listen backlog` - maximum length of the queue for incomming connections;
* `Enter MySQL database server url` - url on which runs your MySQL server;
* `Enter MySQL database server DB name` - database name on your MySQL server, which need to be created by yourself. Collation type is `utf8_general_ci`;
* `Enter MySQL database server username` - username of your MySQL server user;
* `Enter MySQL database server password` - password of your MySQL server user.

### Database configuration

Open your MySQL Server Admin Panel (phpMyAdmin recommended) and import file `UsersData.sql`.


## Quick server starup

Open `server.bat` if you are on Windows or run command `java -jar server.jar` in you are on Linux or OS X.
If all OK, you'll see the message `INFO: The chat server is running`.


## License

`chat`'s code uses the GNU license.
