package Server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	Properties prop = new Properties();

	public int getServerPort() {
		return serverPort;
	}

	public static void setServerPort(int serverPort) {
		ConfigFileReader.serverPort = serverPort;
	}

	public int getServerBacklog() {
		return serverBacklog;
	}

	public void setServerBacklog(int serverBacklog) {
		ConfigFileReader.serverBacklog = serverBacklog;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		ConfigFileReader.serverIp = serverIp;
	}

	public String getDBurl() {
		return dbUrl;
	}

	public void setDBurl(String dbUrl) {
		ConfigFileReader.dbUrl = dbUrl;
	}

	public String getDBName() {
		return dbName;
	}

	public void setDBName(String dbName) {
		ConfigFileReader.dbName = dbName;
	}

	public String getDBUsername() {
		return dbUsername;
	}

	public void setDBUsername(String dbUsername) {
		ConfigFileReader.dbUsername = dbUsername;
	}

	public String getDBPassword() {
		return dbPassword;
	}

	public void setDBPassword(String dbPassword) {
		ConfigFileReader.dbPassword = dbPassword;
	}

	// create server properties value
	private static int serverPort;
	private static int serverBacklog;
	private static String serverIp;

	// create database properties value
	private static String dbUrl;
	private static String dbName;
	private static String dbUsername;
	private static String dbPassword;

	public void loadDataFromFile() throws FileNotFoundException, IOException {
		prop.load(new FileInputStream("config/config.properties"));
	}

	public void readServerProperties() {

		// get the property value and print it out

		serverPort = Integer.parseInt(prop.getProperty("serverPort"));
		setServerPort(serverPort);
		serverBacklog = Integer.parseInt(prop.getProperty("serverBackLog"));
		setServerBacklog(serverBacklog);

		serverIp = prop.getProperty("serverIp");
		setServerIp(serverIp);
	}

	public void readDatabaseProperties() {

		// get the property value and print it out

		dbUrl = prop.getProperty("dbUrl");
		setDBurl(dbUrl);

		dbName = prop.getProperty("dbName");
		setDBName(dbName);

		dbUsername = prop.getProperty("dbUsername");
		setDBUsername(dbUsername);

		dbPassword = prop.getProperty("dbPassword");
		setDBPassword(dbPassword);

	}

	public void defaultServerProperties() {

		// get the property value and print it out

		serverPort = 1234;
		setServerPort(serverPort);
		serverBacklog = 50;
		setServerBacklog(serverBacklog);
		serverIp = "localhost";
		setServerIp(serverIp);

	}

	public void defaultDatabaseProperties() {

		// get the property value and print it out

		dbUrl = "localhost";
		setDBurl(dbUrl);
		dbName = "UsersData";
		setDBName(dbName);
		dbUsername = "root";
		setDBUsername(dbUsername);
		dbPassword = "\"\"";
		setDBPassword(dbPassword);

	}
}
