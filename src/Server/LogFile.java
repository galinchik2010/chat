package Server;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogFile {

	private static Logger logger = Logger.getLogger(Server.class.getName());

	public Logger getLogger() {
		return logger;
	}

	public void createLogger() {
		try {
			FileHandler fh = new FileHandler("log/ServerLogFile");
			logger.addHandler(fh);
			HtmlFormatter htmlformatter = new HtmlFormatter();
			FileHandler htmlfile = new FileHandler("log/ServerLogFile.htm");
			htmlfile.setFormatter(htmlformatter);
			logger.addHandler(htmlfile);

		} catch (SecurityException e) {
			logger.log(Level.SEVERE,
					"Unable to create a log file because of security policy.",
					e);
		} catch (IOException e) {
			logger.log(Level.SEVERE,
					"Unable to create a log file because of an input-output.",
					e);
		}
	}

}
