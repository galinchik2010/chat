package Server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;

public class MySQLconnect {
	private Connection conn = null;
	private Statement st = null;
	private ResultSet rs = null;
	private static ConfigFileReader reader = new ConfigFileReader();
	private String dbUrl = null;
	private String dbName = null;
	private String dbUsername = null;
	private String dbPassword = null;

	private static LogFile log = new LogFile();

	public void connect() {
		try {
			reader.loadDataFromFile();
			reader.readDatabaseProperties();
		} catch (FileNotFoundException e) {
			log.getLogger()
					.log(Level.WARNING,
							"Cannot find database properties file. Work on default parameters");
			reader.defaultDatabaseProperties();

		} catch (IOException e) {
			log.getLogger().log(Level.SEVERE,
					"Cannot read properties file: " + e.getMessage());
		}
		dbUrl = reader.getDBurl();
		dbName = reader.getDBName();
		dbUsername = reader.getDBUsername();
		dbPassword = reader.getDBPassword();

		if (dbPassword.equals("\"\"")) {

			log.getLogger().log(
					Level.INFO,
					"Database parameters: url - " + dbUrl + ", " + "name - "
							+ dbName + ", " + "username - " + dbUsername + ", "
							+ "password - " + "\"\"");
			dbPassword = "";
		} else {

			log.getLogger().log(
					Level.INFO,
					"Database parameters: url - " + dbUrl + ", " + "name - "
							+ dbName + ", " + "username - " + dbUsername + ", "
							+ "password - " + dbPassword);
		}
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + dbUrl + "/"
					+ dbName, dbUsername, dbPassword);
			st = conn.createStatement();
		} catch (Exception e) {
			log.getLogger().log(Level.SEVERE,
					"Cannot connect to database " + e.getMessage());
		}

	}

	public void closeConnection() {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				log.getLogger().log(Level.SEVERE,
						"Error closing database connection: " + e.getMessage());
			}
		}
	}

	public boolean update(String name, String password) throws SQLException {
		String query = "SELECT *FROM `"
				+ dbName
				+ "`.`CHAT_MEMBERS_LIST` WHERE `CHAT_MEMBERS_LIST`.`User name` = "
				+ "'" + name + "'";
		rs = st.executeQuery(query);
		if (!rs.next()) {
			try {
				query = "INSERT INTO `"
						+ dbName
						+ "`.`CHAT_MEMBERS_LIST` (`CHAT_MEMBERS_LIST`.`User name`,`CHAT_MEMBERS_LIST`.`User password`) VALUES("
						+ "'" + name + "'" + "," + "'" + password + "'" + ");";
				st.executeUpdate(query);
			} catch (Exception e) {
				log.getLogger().log(Level.SEVERE, "cannot write to database!");
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean select(String name, String password) throws SQLException {
		String query = "SELECT *FROM `"
				+ dbName
				+ "`.`CHAT_MEMBERS_LIST` WHERE `CHAT_MEMBERS_LIST`.`User name` = "
				+ "'" + name + "'"
				+ " AND `CHAT_MEMBERS_LIST`.`User password` = " + "'"
				+ password + "'";
		rs = st.executeQuery(query);
		if (!rs.next()) {
			return false;
		} else {
			return true;
		}
	}
}
