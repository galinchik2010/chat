package Server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import Json.AbstractChatEntity;
import Json.ChatCommandsEnum;
import Json.ChatEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class that implements server application
 * 
 * @author Sisters
 * 
 */
public class Server {

	private static LogFile log = new LogFile();
	private static MySQLconnect conn = new MySQLconnect();
	private static ServerSocket socket;
	private static List<String> names = new ArrayList<String>();

	private static ConfigFileReader reader = new ConfigFileReader();
	/**
	 * Create a ConcurrentHashMap of client connections
	 */
	private static ConcurrentHashMap<String, PrintWriter> writers = new ConcurrentHashMap<String, PrintWriter>();

	/**
	 * Try to connect with a client and creating a ClientHandler
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int serverPort;
		int serverBacklog;
		String serverIp;
		Locale.setDefault(new Locale("en", "US"));
		log.createLogger();
		try {
			reader.loadDataFromFile();
			reader.readServerProperties();
		} catch (FileNotFoundException e1) {
			reader.defaultServerProperties();
			log.getLogger()
					.log(Level.WARNING,
							"Cannot find server properties file. Work on default parameters");
		} catch (IOException e1) {
			log.getLogger().log(Level.SEVERE,
					"Cannot read properties file: " + e1.getMessage());
		}
		serverPort = reader.getServerPort();
		serverBacklog = reader.getServerBacklog();
		serverIp = reader.getServerIp();
		log.getLogger().log(
				Level.INFO,
				"Server parameters: port - " + serverPort + ", " + "backlog - "
						+ serverBacklog + ", " + "ip - " + serverIp);
		try {

			socket = new ServerSocket(serverPort, serverBacklog,
					InetAddress.getByName(serverIp));
			conn.connect();
			log.getLogger().log(Level.INFO, "The chat server is running ");

			while (true) {
				new Thread(new ClientHandler(socket.accept())).start();
			}
		} catch (IOException e) {
			log.getLogger().log(Level.SEVERE,
					"Error creating socket: " + e.getMessage());

		} catch (Exception e) {
			log.getLogger().log(Level.SEVERE,
					"Cannot connect to database " + e.getMessage());
			return;
		} finally {
			try {
				socket.close();
				log.getLogger().log(Level.INFO, "Server connection terminated");
				conn.closeConnection();
				log.getLogger().log(Level.INFO,
						"Database connection terminated");
			} catch (IOException e) {
				log.getLogger().log(Level.SEVERE,
						"Error closing socket: " + e.getMessage());
			}
		}
	}

	/**
	 * Class to updater the list of chat members and check for uniqueness of the
	 * client nickname
	 * 
	 */
	private static class MembersUpdater {

		/**
		 * If member left send info message to all clients
		 * 
		 * @param member
		 */
		public static void sendLeftMemberMessage(String member) {
			String temp = null;
			ObjectMapper om = new ObjectMapper();

			AbstractChatEntity chatEntity = new AbstractChatEntity();
			chatEntity.setCommand(ChatCommandsEnum.INFO_MESSAGE);
			chatEntity.setObject("User " + member + " left.");

			try {
				temp = om.writeValueAsString(chatEntity);
			} catch (Exception e) {
				log.getLogger().log(Level.SEVERE,
						"Cannot write a message to client " + e.getMessage());
			}

			for (String nick : writers.keySet()) {
				writers.get(nick).println(temp);
			}
		}

		/**
		 * If joined a new member, send info message to all clients
		 * 
		 * @param member
		 */
		public static void sendJoinedMemberMessage(String member) {
			String temp = null;
			ObjectMapper om = new ObjectMapper();

			AbstractChatEntity chatEntity = new AbstractChatEntity();
			chatEntity.setCommand(ChatCommandsEnum.INFO_MESSAGE);
			chatEntity.setObject("User " + member + " joined.");

			try {
				temp = om.writeValueAsString(chatEntity);
			} catch (Exception e) {
				e.printStackTrace();
			}

			for (String nick : writers.keySet()) {
				writers.get(nick).println(temp);
			}
		}

		/**
		 * Updated list of members: delete members from list, if he left and add
		 * members to the list if he joined
		 */
		public static void sendUpdatedMemberList() {
			AbstractChatEntity chatEntity = new AbstractChatEntity();
			chatEntity.setCommand(ChatCommandsEnum.MEMBERS_LIST_RESPONSE);
			String temp = null;
			List<String> chatMembers = new ArrayList<String>(writers.keySet());

			chatEntity.setObject(chatMembers);

			ObjectMapper om = new ObjectMapper();
			try {
				temp = om.writeValueAsString(chatEntity);
			} catch (Exception e) {
				log.getLogger().log(Level.SEVERE,
						"Cannot write a message to clients " + e.getMessage());
			}

			for (String nick : writers.keySet()) {
				writers.get(nick).println(temp);
			}
		}
	}

	/**
	 * Create ClientHander
	 */
	private static class ClientHandler implements Runnable {
		private Socket clientSocket;
		private BufferedReader in = null;
		private PrintWriter out;
		private String memberName;
		private String nickname;
		private String password;

		public ClientHandler(Socket clientSocket) {
			this.clientSocket = clientSocket;
		}

		@Override
		public void run() {

			/**
			 * Try to create input and output streams
			 */
			try {
				// Create streams for the socket.
				in = new BufferedReader(new InputStreamReader(
						clientSocket.getInputStream()));
				out = new PrintWriter(clientSocket.getOutputStream(), true);
			} catch (Exception e) {
				log.getLogger().log(Level.SEVERE,
						"Error creating streams: " + e.getMessage());
			}

			String jsonString = null;

			/**
			 * reading data from client output stream
			 */
			while (true) {

				try {
					jsonString = in.readLine();

				} catch (Exception e) {
					log.getLogger().log(Level.INFO,
							"User " + memberName + " left.");
					if (memberName != null) {
						names.remove(memberName);
						writers.remove(memberName);

						MembersUpdater.sendLeftMemberMessage(memberName);
						MembersUpdater.sendUpdatedMemberList();
					}

					return;
				}

				/**
				 * parse json string from client
				 */
				if (jsonString != null) {
					ObjectMapper mapper = new ObjectMapper();

					AbstractChatEntity absChat = null;
					try {
						absChat = mapper.readValue(jsonString,
								AbstractChatEntity.class);
					} catch (Exception e) {
						log.getLogger().log(
								Level.SEVERE,
								"Cannot read a message from a client "
										+ e.getMessage());
					}

					/**
					 * Get command from a json string and implement actions for
					 * each command
					 */
					switch (absChat.getCommand()) {
					/**
					 * Authorize a client on the server
					 */
					case ChatCommandsEnum.REGISTRATION_REQUEST: {
						ChatEntity chatMessageEntity;
						try {
							chatMessageEntity = getChatMessageEntity(absChat);
							nickname = chatMessageEntity.getUsername();
							password = chatMessageEntity.getPassword();

						} catch (IOException e) {
							log.getLogger().log(
									Level.SEVERE,
									"Cannot parse a message from a client "
											+ e.getMessage());
						}

						try {
							if (conn.update(nickname, password)) {
								AbstractChatEntity authError = new AbstractChatEntity();
								authError
										.setCommand(ChatCommandsEnum.REGISTRATION_RESPONSE);
								authError.setObject(new Boolean(true));

								/**
								 * Try to parsing a string
								 */
								try {
									jsonString = mapper
											.writeValueAsString(authError);
								} catch (Exception e) {
									log.getLogger().log(
											Level.SEVERE,
											"Cannot write a message to a client "
													+ e.getMessage());
								}

								/**
								 * Send to the client a json string
								 */
								out.println(jsonString);

							} else {
								AbstractChatEntity authError = new AbstractChatEntity();
								authError
										.setCommand(ChatCommandsEnum.REGISTRATION_RESPONSE);
								authError.setObject(new Boolean(false));

								try {
									jsonString = mapper
											.writeValueAsString(authError);

								} catch (Exception e) {
									log.getLogger().log(Level.SEVERE,
											"Error! " + e.getMessage());
								}

								out.println(jsonString);
							}

						} catch (Exception e) {
							log.getLogger().log(Level.SEVERE,
									"Error! " + e.getMessage());
						}
					}
						break;

					case ChatCommandsEnum.AUTHORIZE_REQUEST: {
						try {
							ChatEntity chatMessageEntity = getChatMessageEntity(absChat);
							nickname = chatMessageEntity.getUsername();
							password = chatMessageEntity.getPassword();
						} catch (IOException e1) {
							log.getLogger().log(
									Level.SEVERE,
									"Cannot parse a message from a client "
											+ e1.getMessage());
						}

						try {
							if (conn.select(nickname, password)) {
								if (names.contains(nickname)) {
									AbstractChatEntity authError = new AbstractChatEntity();
									authError
											.setCommand(ChatCommandsEnum.AUTHORIZE_RESPONSE);
									authError
											.setObject(ChatCommandsEnum.AUTHORIZE_SAME_NAME);

									/**
									 * Try to parsing a string
									 */
									jsonString = mapper
											.writeValueAsString(authError);

									/**
									 * Send to the client a json string
									 */
									out.println(jsonString);
								}
								AbstractChatEntity authError = new AbstractChatEntity();
								authError
										.setCommand(ChatCommandsEnum.AUTHORIZE_RESPONSE);
								authError
										.setObject(ChatCommandsEnum.AUTHORIZE_TRUE);
								log.getLogger().log(Level.INFO,
										"Got user " + nickname);

								/**
								 * Try to parsing a string
								 */
								jsonString = mapper
										.writeValueAsString(authError);

								/**
								 * Send to the client a json string
								 */
								out.println(jsonString);

								memberName = nickname;
								names.add(memberName);
								writers.put(nickname, out);

								MembersUpdater
										.sendJoinedMemberMessage(memberName);
								MembersUpdater.sendUpdatedMemberList();
							}
							/**
							 * Parsing a message about authorize error
							 */
							else {
								AbstractChatEntity authError = new AbstractChatEntity();
								authError
										.setCommand(ChatCommandsEnum.AUTHORIZE_RESPONSE);
								authError
										.setObject(ChatCommandsEnum.AUTHORIZE_FALSE);

								try {
									jsonString = mapper
											.writeValueAsString(authError);

								} catch (Exception e) {
									log.getLogger().log(Level.SEVERE,
											"Error! " + e.getMessage());
								}

								out.println(jsonString);

								/**
								 * Try to stop on 2 sec a client thread
								 */
								try {
									Thread.sleep(2000);
								} catch (Exception e) {
									log.getLogger().log(Level.SEVERE,
											"Error! " + e.getMessage());
								} finally {
									try {
										in.close();
										out.close();

									} catch (IOException e) {
										log.getLogger().log(Level.SEVERE,
												"Error! " + e.getMessage());
									}
								}

								return;
							}
						} catch (SQLException e) {
							log.getLogger().log(Level.SEVERE, e.getMessage());
						} catch (JsonProcessingException e) {

							log.getLogger().log(
									Level.SEVERE,
									"Cannot write a message to a client "
											+ e.getMessage());

						}
					}
						break;

					/**
					 * Send all client threads a chat message
					 */
					case ChatCommandsEnum.CHAT_MESSAGE: {

						for (String nick : writers.keySet()) {
							writers.get(nick).println(jsonString);
						}
					}
						break;
					}
				}
			}
		}

		@SuppressWarnings("unused")
		public AbstractChatEntity getAbstractClientMessage() throws IOException {
			String jsonString = null;
			ObjectMapper mapper = new ObjectMapper();

			jsonString = in.readLine();

			if (jsonString != null) {
				return mapper.readValue(jsonString, AbstractChatEntity.class);
			}

			return null;
		}

		/**
		 * Parsing the message from json string
		 * 
		 * @param absChat
		 * @return
		 * @throws IOException
		 */
		public ChatEntity getChatMessageEntity(AbstractChatEntity absChat)
				throws IOException {
			ObjectMapper mapper = new ObjectMapper();

			String jsonString = mapper.writeValueAsString(absChat);

			JsonNode objectNode = mapper.readTree(jsonString).get("object");
			ChatEntity chatMessageEntity = mapper.readValue(
					objectNode.toString(), ChatEntity.class);

			return chatMessageEntity;
		}
	}

}
