package Server;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class HtmlFormatter extends Formatter {

	public HtmlFormatter() {

	}

	/**
	 * Return the header part of the HTML file.
	 */
	@Override
	public String getHead(Handler h) {
		/**
		 * Write the HTML header file, the meta information and the beginning of
		 * the table.
		 */
		return "<html><head><title>AppLog</title>"
				+ "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">"
				+ "</head><body>"
				+ "<table border=1>"
				+ "<tr bgcolor=CYAN><td>date</td><td>level</td><td>class</td><td>method</td>"
				+ "<td>message</td><td>thrown message</td><td>stacktrace</td></tr>";
	}

	/**
	 * Return the end of the HTML file.
	 */
	@Override
	public String getTail(Handler h) {
		/**
		 * Write the end of the table and the end of the HTML file.
		 */
		return "</table></body></html>";
	}

	/**
	 * Format the same message in a table row.
	 */
	@Override
	public String format(LogRecord record) {
		StringBuilder result = new StringBuilder();
		Date d = new Date();
		Level level = record.getLevel();

		/**
		 * Errors are highlighted in red, Warning - gray News reports - white.
		 */
		if (level == Level.SEVERE) {
			result.append("<tr bgColor=Tomato><td>");
		} else if (level == Level.WARNING) {
			result.append("<tr bgColor=GRAY><td>");
		} else {
			result.append("<tr bgColor=WHITE><td>");
		}

		result.append("\n");

		result.append(d);
		result.append("</td><td>");
		result.append(record.getLevel().toString());
		result.append("</td><td>");
		result.append(record.getSourceClassName());
		result.append("</td><td>");
		result.append(record.getSourceMethodName());
		result.append("</td><td>");
		result.append(record.getMessage());
		result.append("</td><td>");

		Throwable thrown = record.getThrown();

		if (thrown != null) {
			// If an exception was given, list complete
			// call stack.
			result.append(record.getThrown().getMessage());
			result.append("</td><td>");

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			record.getThrown().printStackTrace(pw);
			String stackTrace = sw.toString();

			result.append(stackTrace);
			result.append("</td>");
		} else {
			result.append("</td><td>null");
			result.append("</td>");
		}

		// End of Line
		result.append("</tr>\n");
		return result.toString();
	}
}
