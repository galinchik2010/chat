package Client;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import Json.AbstractChatEntity;
import Json.ChatCommandsEnum;
import Json.ChatEntity;

/**
 * Class that controls the interaction between the main chat and the clientModel
 * 
 * @author Halyna Mudryk
 */
public class ChatController {
	private static ChatView chatView;
	private static LoginView loginView;

	/**
	 * Constructor that initializes the objects of classes LoginView and
	 * ChatView and adds the listener of Send button
	 * 
	 * @param chatView
	 * @param loginView
	 */
	public ChatController(ChatView chatView, LoginView loginView) {
		ChatController.chatView = chatView;
		ChatController.loginView = loginView;
		ChatController.chatView.addSendButtonListener(new SendListener());
		ChatController.chatView.addSendEnterListener(new SendListener());
		ChatController.chatView.addExitListener(new ExitListener());
	}

	/**
	 * Inner class that send commands to the Client model
	 * 
	 */

	class SendListener extends KeyAdapter implements ActionListener {
		private String nick;
		private String message;
		private String time;

		public void getData() {
			Calendar cal = Calendar.getInstance();
			cal.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			time = sdf.format(cal.getTime()).toString();
			message = chatView.getMessage();
			if (message.trim().equals(""))
				return;
			chatView.messageField.setText("");
			nick = chatView.getNickname();
			sendMessage(time, nick, message);
			chatView.addChatMessage(time, nick, message, true);
		}

		/**
		 * Function that send commands to the client model to update the model's
		 * state for send messages to the server and receive theirs from the
		 * server
		 */
		public void actionPerformed(ActionEvent e) {
			getData();
		}

		public void keyPressed(KeyEvent e) {
			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
				getData();
			}
		}

		/**
		 * Function in which caused the function from the client model with
		 * nickname and message parameters
		 * 
		 * @param nick
		 * @param message
		 */
		private void sendMessage(String time, String nick, String message) {

			try {

				ClientModel.sendMessage(time, nick, message);
			} catch (Exception e) {
				chatView.displayErrorMessage("Error sending message: "
						+ e.getMessage());
			}
		}

	}

	/**
	 * Class in which caused methods from client model and main chat window and
	 * realized the exchange of information between them
	 */
	public static class HandleMessages implements Runnable {
		@SuppressWarnings("unchecked")
		/**
		 * Recognize messages from server  on a command
		 * and add them to the appropriate fields of main chat window
		 */
		public void run() {
			try {
				AbstractChatEntity abstractChatMessage;

				while (true) {
					abstractChatMessage = ClientModel
							.getAbstractServerMessage();

					if (abstractChatMessage != null) {

						switch (abstractChatMessage.getCommand()) {
						case ChatCommandsEnum.INFO_MESSAGE: {
							String message = (String) abstractChatMessage
									.getObject();
							chatView.addInfoMessage(message);

						}
							break;

						case ChatCommandsEnum.CHAT_MESSAGE: {
							ChatEntity chatMessageEntity = ClientModel
									.getChatMessageEntity(abstractChatMessage);

							String messageNick = chatMessageEntity.getUsername();

							if (!messageNick.equals(loginView.getNickName()))
								chatView.addChatMessage(
										chatMessageEntity.getTime(),
										chatMessageEntity.getUsername(),
										chatMessageEntity.getMessage(), false);
						}
							break;

						case ChatCommandsEnum.MEMBERS_LIST_RESPONSE: {
							chatView.setChatMembers((List<String>) abstractChatMessage
									.getObject());
						}
							break;
						}
					}
				}
			} catch (Exception e) {
				chatView.setVisible(false);
				chatView.displayErrorMessage(e.getMessage());
				loginView.setVisible(true);

			}
		}
	}

	class ExitListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			chatView.setVisible(false);
			System.exit(1);
		}
	}
}
