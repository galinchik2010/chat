package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;

/**
 * This class implements the visualization of the authorization to chat
 * 
 * @author Halyna Mudryk
 */
@SuppressWarnings("serial")
public class RegisterView extends JFrame {
	/**
	 * Declares the elements of JFRAME
	 */

	protected JLabel addressLabel;
	protected JLabel portLabel;
	protected JLabel nickNameLabel;
	protected JLabel passwordLabel;
	protected JLabel instruction;
	protected JButton registerButton;
	protected JButton exitButton;
	protected JTextField addressField;
	protected JTextField portField;
	protected JTextField nickNameField;
	protected JPasswordField passwordField;

	public RegisterView() {

		/**
		 * Specify the parameters of the window elements
		 */
		Font font = new Font("Verdana", Font.BOLD, 12);

		addressLabel = new JLabel("Address:");
		addressLabel.setForeground(Color.gray);
		addressLabel.setFont(font);

		portLabel = new JLabel("Port:");
		portLabel.setForeground(Color.gray);
		portLabel.setFont(font);

		nickNameLabel = new JLabel("Nickname:");
		nickNameLabel.setForeground(Color.gray);
		nickNameLabel.setFont(font);

		passwordLabel = new JLabel("Password:");
		passwordLabel.setForeground(Color.gray);
		passwordLabel.setFont(font);

		instruction = new JLabel("How to write the password");
		instruction.setForeground(Color.blue);
		instruction.setFont(new Font("Verdana", Font.BOLD, 8));
		instruction
				.setToolTipText("- The length of password from minimum 6 letters to maximum 15 letters.");
		instruction.setCursor(new Cursor(Cursor.HAND_CURSOR));

		addressField = new JTextField();
		addressField.setDocument(new JTextFieldLimit(15));

		portField = new JTextField();
		portField.setDocument(new JTextFieldLimit(5));

		nickNameField = new JTextField();
		nickNameField.setDocument(new JTextFieldLimit(15));

		passwordField = new JPasswordField();
		passwordField.setDocument(new JTextFieldLimit(15));
		passwordField.setEchoChar('*');

		registerButton = new JButton("Register");
		registerButton.setForeground(Color.getHSBColor(0.33f, 0.76f, 0.55f));
		registerButton.setFont(font);

		exitButton = new JButton("Exit");
		exitButton.setForeground(Color.red);
		exitButton.setFont(font);

		addressField.setPreferredSize(new Dimension(150, 20));

		/**
		 * Use grouplayout to horizontally and vertically align the elements of
		 * JFrame
		 */
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		/**
		 * Set horizontalGroup of the grouplayout
		 */
		layout.setHorizontalGroup(layout
				.createSequentialGroup()
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(addressLabel)
								.addComponent(portLabel)
								.addComponent(nickNameLabel)
								.addComponent(passwordLabel))

				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(addressField)
								.addComponent(portField)
								.addComponent(nickNameField)
								.addComponent(passwordField)
								.addComponent(instruction))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(registerButton)
								.addComponent(exitButton)));
		/**
		 * Set vertical group of the grouplayout
		 */
		layout.setVerticalGroup(layout
				.createSequentialGroup()
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(addressLabel)
								.addComponent(addressField)
								.addComponent(registerButton))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(portLabel)
								.addComponent(portField)
								.addComponent(exitButton))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(nickNameLabel)
								.addComponent(nickNameField))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(passwordLabel)
								.addComponent(passwordField))
				.addComponent(instruction));
		layout.linkSize(SwingConstants.HORIZONTAL, addressField, portField,
				nickNameField, passwordField);
		layout.linkSize(SwingConstants.HORIZONTAL, registerButton, exitButton);

		/**
		 * Set options of the Window
		 */
		setTitle("Registration");
		getContentPane().setBackground(Color.getHSBColor(0.5f, 0.28f, 0.95f));
		ImageIcon icon = new ImageIcon("icon.png");
		this.setIconImage(icon.getImage());

		pack();
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
	}

	/**
	 * Get address that the client entered to the address JTextField
	 * 
	 * @return addressField.getText();
	 */
	public String getAddress() {
		return addressField.getText();
	}

	/**
	 * Get port that the client entered to the port JTextField
	 * 
	 * @return Integer.parseInt(portField.getText())
	 */
	public int getPort() {
		return Integer.parseInt(portField.getText());
	}

	/**
	 * Get nickname that the client entered to the nickname JTextField
	 * 
	 * @return nickNameField.getText()
	 */
	public String getNickName() {
		return nickNameField.getText();
	}

	public char[] getPass() {
		return passwordField.getPassword();
	}

	/**
	 * Add listener for the button of authorization witch calls Sign In
	 * 
	 * @param listenForRegisterButton
	 */
	void addRegisterListener(ActionListener listenForRegisterButton) {
		registerButton.addActionListener(listenForRegisterButton);
	}

	void addRegisterEnterListener(KeyAdapter listenForEnterKey) {

		addressField.addKeyListener(listenForEnterKey);
		portField.addKeyListener(listenForEnterKey);
		nickNameField.addKeyListener(listenForEnterKey);
		passwordField.addKeyListener(listenForEnterKey);
	}

	void addInstructionListener(MouseAdapter listenForInstructionLabel) {
		instruction.addMouseListener(listenForInstructionLabel);
	}

	/**
	 * Add listener for the exit button
	 * 
	 * @param listenForExitButton
	 */
	void addExitListener(ActionListener listenForExitButton) {
		exitButton.addActionListener(listenForExitButton);
	}

	/**
	 * Function witch displays different connection errors between client and
	 * server
	 * 
	 * @param errorMessage
	 */
	void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}

	void displayInfoMessage(String infoMessage) {
		JOptionPane.showMessageDialog(this, infoMessage);
	}

}
