package Client;

import static javax.swing.GroupLayout.Alignment.BASELINE;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.util.List;
import javax.swing.border.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.JPanel;
import javax.swing.JTextPane;

/**
 * This class implements the visualization of the main chat
 * 
 * @author Halyna Mudryk
 */
@SuppressWarnings("serial")
public class ChatView extends JFrame {

	/**
	 * Declares the elements of JFRAME
	 */
	protected JTextPane messagesArea;
	protected JPanel topPanel;
	protected JTextArea namesArea;
	protected JButton sendButton;
	protected JButton exitButton;
	protected JTextField messageField;
	protected JLabel ChatLabel;
	protected JLabel NamesLabel;
	protected String nickname;
	protected JScrollPane messagesScroll;
	protected JScrollPane namesScroll;

	/**
	 * Take and return the nickname of the client
	 * 
	 * @return nickname;
	 */
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * Specify the parameters of the window elements
	 */
	public ChatView() {

		topPanel = new JPanel(new BorderLayout());
		topPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createBevelBorder(BevelBorder.LOWERED),
				BorderFactory.createEmptyBorder()));

		ChatLabel = new JLabel("Messages");
		ChatLabel.setForeground(Color.BLACK);

		NamesLabel = new JLabel("List of members");
		NamesLabel.setForeground(Color.BLACK);

		messagesArea = new JTextPane();
		messagesScroll = new JScrollPane(messagesArea);
		messagesScroll.setPreferredSize(new Dimension(600, 200));
		messagesScroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		topPanel.add(messagesScroll, BorderLayout.CENTER);
		namesArea = new JTextArea();
		namesArea.setEditable(false);
		namesArea.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createBevelBorder(BevelBorder.LOWERED),
				BorderFactory.createEmptyBorder()));

		namesScroll = new JScrollPane(namesArea);
		namesScroll.setPreferredSize(new Dimension(100, 200));
		namesScroll
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		messageField = new JTextField();

		sendButton = new JButton("Send");
		sendButton.setForeground(Color.getHSBColor(180, 238, 180));

		exitButton = new JButton("Exit");
		exitButton.setForeground(Color.RED);

		/**
		 * Use grouplayout to horizontally and vertically align the elements of
		 * JFrame
		 */
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		/**
		 * Set horizontalGroup of the grouplayout
		 */
		layout.setHorizontalGroup(layout
				.createSequentialGroup()
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(ChatLabel).addComponent(topPanel)
								.addComponent(messageField))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(NamesLabel)
								.addComponent(namesScroll)
								.addGroup(
										layout.createSequentialGroup()
												.addComponent(sendButton)
												.addComponent(exitButton))));
		/**
		 * Set vertical group of the grouplayout
		 */
		layout.setVerticalGroup(layout
				.createSequentialGroup()
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(ChatLabel)
								.addComponent(NamesLabel))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(topPanel)
								.addComponent(namesScroll))
				.addContainerGap()

				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(messageField)
								.addGroup(
										layout.createParallelGroup(BASELINE)
												.addComponent(sendButton)
												.addComponent(exitButton))));

		/**
		 * Set options of the Window
		 */
		ImageIcon icon = new ImageIcon("images/icon.png");
		this.setIconImage(icon.getImage());
		Color color = Color.getHSBColor(0.1f, 0.2f, 1f);
		getContentPane().setBackground(color);
		topPanel.setBackground(color);

		pack();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
	}

	/**
	 * Get message that the client entered to the message JTextField
	 * 
	 * @return
	 */
	public String getMessage() {
		return messageField.getText();
	}

	/**
	 * Set the client nickname
	 */
	public void setName(String data) {
		namesArea.setText(data);
	}

	/**
	 * Set the title of the frame with the current client nickname
	 */
	@Override
	public void setTitle(String name) {
		super.setTitle("ChatOk - [" + name + "]");
		nickname = name;
	}

	/**
	 * Set the list of chat members to the names TextArea
	 * 
	 * @param listIn
	 */
	public void setChatMembers(List<String> listIn) {
		String members = "";

		for (String name : listIn)
			members = members + name + "\n";
		namesArea.setForeground(Color.ORANGE);
		namesArea.setText(members);
	}

	/**
	 * Add the client massage to the general chat window
	 * 
	 * @param nick
	 * @param message
	 * @param isMine
	 */
	public void addChatMessage(String time, String nick, String message,
			Boolean isMine) {
		String windowMessage;
		messagesArea.setEditable(true);
		if (isMine) {
			windowMessage = time + " My message" + ": " + message;
			appendToPane(messagesArea, windowMessage + "\n", Color.DARK_GRAY);
		} else {
			windowMessage = time + " " + nick + ": " + message;
			appendToPane(messagesArea, windowMessage + "\n", Color.GRAY);
		}
		messagesArea.setEditable(false);
	}

	/**
	 * Add information message to the general chat window
	 * 
	 * @param message
	 */
	public void addInfoMessage(String message) {
		messagesArea.setEditable(true);
		String windowMessage = "INFO: " + message;
		appendToPane(messagesArea, windowMessage + "\n", Color.BLUE);
		messagesArea.setEditable(false);
	}

	/**
	 * Add listener for the Send button
	 * 
	 * @param listenForSendButton
	 */
	void addSendButtonListener(ActionListener listenForSendButton) {
		sendButton.addActionListener(listenForSendButton);
	}

	void addSendEnterListener(KeyAdapter listenForEnterKey) {
		messageField.addKeyListener(listenForEnterKey);
	}

	void addExitListener(ActionListener listenForExitButton) {
		exitButton.addActionListener(listenForExitButton);
	}

	/**
	 * Function witch displays different connection errors between client and
	 * server
	 * 
	 * @param errorMessage
	 */
	void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}

	private void appendToPane(JTextPane tp, String msg, Color c) {
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY,
				StyleConstants.Foreground, c);

		aset = sc.addAttribute(aset, StyleConstants.FontFamily,
				"Lucida Console");
		aset = sc.addAttribute(aset, StyleConstants.Alignment,
				StyleConstants.ALIGN_JUSTIFIED);

		int len = tp.getDocument().getLength();
		tp.setCaretPosition(len);
		tp.setCharacterAttributes(aset, false);
		tp.replaceSelection(msg);
	}

}
