package Client;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseAdapter;

import Json.ChatCommandsEnum;

/**
 * Class that controls the interaction between the window authorization and the
 * clientModel
 * 
 * @author Halyna Mudryk
 * 
 */
public class LoginController {

	private LoginView loginView;
	private ChatView chatView;
	private RegisterView regView;
	private String address;
	private int port;
	private String nick;
	private String password;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Constructor that initializes the objects of classes LoginView and
	 * ChatView and adds the listeners of Sign In and Exit buttons
	 * 
	 * @param loginView
	 * @param chatView
	 */
	public LoginController(LoginView loginView, ChatView chatView,
			RegisterView regView) {
		this.loginView = loginView;
		this.chatView = chatView;
		this.regView = regView;
		this.loginView.addSignInButtonListener(new SignInListener());
		this.loginView.addSignInEnterListener(new SignInListener());
		this.loginView.addExitListener(new ExitListener());
		this.loginView.addRegisterListener(new RegistrationListener());
	}

	/**
	 * Inner class that send commands to the Client model
	 */

	class RegistrationListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			regView.setVisible(true);
		}

	}

	class SignInListener extends KeyAdapter implements ActionListener {

		/**
		 * Variables which we appropriate the values of the text fields
		 */

		public void getData() {
			/**
			 * Check that all fields are filled
			 */
			if (loginView.addressField.getText().trim().equals("")
					|| loginView.portField.getText().trim().equals("")
					|| loginView.nickNameField.getText().trim().equals("")) {
				loginView.displayErrorMessage("Not all fields are filled");
				return;
			}
			/**
			 * Try-catch block for checking the validity of the arguments
			 */
			try {
				address = loginView.getAddress();
				port = loginView.getPort();
				nick = loginView.getNickName();
				password = loginView.getPass();

				if (!ClientModel.connect(address, port)) {
					loginView
							.displayErrorMessage("Coudn't connect to the server.\nCheck address and port.");
					loginView.setVisible(true);
				} else {
					initChat();

				}

			} catch (NumberFormatException ex) {
				loginView.displayErrorMessage("Error! Port must be a number");
			}
		}

		/**
		 * Function that send commands to the client model to update the model's
		 * state for connecting to the server
		 */
		public void actionPerformed(ActionEvent e) {

			getData();
		}

		public void keyPressed(KeyEvent e) {
			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
				getData();
			}
		}

		/**
		 * Function that close authorization window and opened the main chat
		 * window if authorization was successful
		 */
		private void initChat() {
			try {
				/**
				 * If the client is try to connect to the server with the same
				 * name he must enter another name
				 */
				switch (ClientModel.authorizeUser(nick, password)) {
				case ChatCommandsEnum.AUTHORIZE_TRUE: {
					loginView.passwordField.setText("");
					chatView.setVisible(true);
					loginView.setVisible(false);
					chatView.setTitle(nick);
					new Thread(new ChatController.HandleMessages()).start();
				}
					break;
				case ChatCommandsEnum.AUTHORIZE_FALSE: {
					loginView
							.displayErrorMessage("Wrong name or password! Try again.");
					loginView.setVisible(true);
				}
					break;

				case ChatCommandsEnum.AUTHORIZE_SAME_NAME: {
					loginView.displayErrorMessage("The user is already online");
					loginView.setVisible(true);
				}
					break;

				}

			} catch (Exception e) {
				loginView.displayErrorMessage("Error initialising chat: "
						+ e.getMessage());
			}

		}
	}

	/**
	 * Class that realize the closing of the window when clicked Exit button
	 * 
	 * @author Sisters
	 * 
	 */
	class ExitListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			loginView.setVisible(false);
			System.exit(1);
		}
	}
}