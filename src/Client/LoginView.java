package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;

/**
 * This class implements the visualization of the authorization to chat
 * 
 * @author Halyna Mudryk
 */
@SuppressWarnings("serial")
public class LoginView extends JFrame {
	/**
	 * Declares the elements of JFRAME
	 */

	protected JLabel addressLabel;
	protected JLabel portLabel;
	protected JLabel nickNameLabel;
	protected JLabel passwordLabel;
	protected JLabel registerLink;
	protected JLabel portInfo;
	protected JButton signInButton;
	protected JButton exitButton;
	protected JTextField addressField;
	protected JTextField portField;
	protected JTextField nickNameField;
	protected JPasswordField passwordField;

	public LoginView() {

		/**
		 * Specify the parameters of the window elements
		 */
		Font font = new Font("Verdana", Font.BOLD, 12);

		addressLabel = new JLabel("Address:");
		addressLabel.setForeground(Color.gray);
		addressLabel.setFont(font);

		portLabel = new JLabel("Port:");
		portLabel.setForeground(Color.gray);
		portLabel.setFont(font);

		nickNameLabel = new JLabel("Nickname:");
		nickNameLabel.setForeground(Color.gray);
		nickNameLabel.setFont(font);

		passwordLabel = new JLabel("Password:");
		passwordLabel.setForeground(Color.gray);
		passwordLabel.setFont(font);

		portInfo = new JLabel("range: 1024-65535");
		Font infoFont = new Font("Verdana", Font.BOLD, 8);
		portInfo.setForeground(Color.gray);
		portInfo.setFont(infoFont);

		registerLink = new JLabel("Registration");
		registerLink.setForeground(Color.BLUE);
		registerLink.setFont(font);
		registerLink.setCursor(new Cursor(Cursor.HAND_CURSOR));

		addressField = new JTextField();
		addressField.setDocument(new JTextFieldLimit(15));

		portField = new JTextField();
		portField.setDocument(new JTextFieldLimit(5));

		nickNameField = new JTextField();
		nickNameField.setDocument(new JTextFieldLimit(15));

		passwordField = new JPasswordField();
		passwordField.setDocument(new JTextFieldLimit(15));
		passwordField.setEchoChar('*');

		signInButton = new JButton("Sign In");
		signInButton.setForeground(Color.getHSBColor(0.33f, 0.76f, 0.55f));
		signInButton.setFont(font);

		exitButton = new JButton("Exit");
		exitButton.setForeground(Color.red);
		exitButton.setFont(font);

		addressField.setPreferredSize(new Dimension(150, 20));

		/**
		 * Use grouplayout to horizontally and vertically align the elements of
		 * JFrame
		 */
		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);

		/**
		 * Set horizontalGroup of the grouplayout
		 */
		layout.setHorizontalGroup(layout
				.createSequentialGroup()
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(addressLabel)
								.addComponent(portLabel)
								.addComponent(nickNameLabel)
								.addComponent(passwordLabel))

				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(addressField)
								.addComponent(portField)
								.addComponent(nickNameField)
								.addComponent(passwordField)
								.addComponent(registerLink))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.LEADING)
								.addComponent(portInfo)
								.addComponent(signInButton)
								.addComponent(exitButton)));
		/**
		 * Set vertical group of the grouplayout
		 */
		layout.setVerticalGroup(layout
				.createSequentialGroup()
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(addressLabel)
								.addComponent(addressField))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(portLabel)
								.addComponent(portField).addComponent(portInfo))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(nickNameLabel)
								.addComponent(nickNameField)
								.addComponent(signInButton))
				.addGroup(
						layout.createParallelGroup(
								GroupLayout.Alignment.BASELINE)
								.addComponent(passwordLabel)
								.addComponent(passwordField)
								.addComponent(exitButton))
				.addComponent(registerLink));

		layout.linkSize(SwingConstants.HORIZONTAL, addressField, portField,
				nickNameField, passwordField);
		layout.linkSize(SwingConstants.HORIZONTAL, signInButton, exitButton);

		/**
		 * Set options of the Window
		 */
		setTitle("Authorization");
		getContentPane().setBackground(Color.getHSBColor(0.22f, 0.76f, 1.0f));
		ImageIcon icon = new ImageIcon("images/icon.png");
		this.setIconImage(icon.getImage());

		pack();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
	}

	/**
	 * Get address that the client entered to the address JTextField
	 * 
	 * @return addressField.getText();
	 */
	public String getAddress() {
		return addressField.getText();
	}

	/**
	 * Get port that the client entered to the port JTextField
	 * 
	 * @return Integer.parseInt(portField.getText())
	 */
	public int getPort() {
		return Integer.parseInt(portField.getText());
	}

	/**
	 * Get nickname that the client entered to the nickname JTextField
	 * 
	 * @return nickNameField.getText()
	 */
	public String getNickName() {
		return nickNameField.getText();
	}

	public String getPass() {
		return String.valueOf(passwordField.getPassword());
	}

	/**
	 * Add listener for the button of authorization witch calls Sign In
	 * 
	 * @param listenForSignInButton
	 */
	void addSignInButtonListener(ActionListener listenForSignInButton) {
		signInButton.addActionListener(listenForSignInButton);
	}

	void addRegisterListener(MouseAdapter listenForRegisterLabel) {
		registerLink.addMouseListener(listenForRegisterLabel);
	}

	void addSignInEnterListener(KeyAdapter listenForEnterKey) {

		addressField.addKeyListener(listenForEnterKey);
		portField.addKeyListener(listenForEnterKey);
		nickNameField.addKeyListener(listenForEnterKey);
		passwordField.addKeyListener(listenForEnterKey);
	}

	/**
	 * Add listener for the exit button
	 * 
	 * @param listenForExitButton
	 */
	void addExitListener(ActionListener listenForExitButton) {
		exitButton.addActionListener(listenForExitButton);
	}

	/**
	 * Function witch displays different connection errors between client and
	 * server
	 * 
	 * @param errorMessage
	 */
	void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}

}
