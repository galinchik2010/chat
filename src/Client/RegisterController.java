package Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterController {
	private RegisterView regView;
	private String address;
	private int port;
	private String nick;
	private String password;

	public RegisterController(RegisterView regView) {
		this.regView = regView;
		this.regView.addExitListener(new ExitListener());
		this.regView.addRegisterListener(new RegisterListener());
		this.regView.addRegisterEnterListener(new RegisterListener());
	}

	public boolean checkPasswordValidity(String password) {
		Pattern p = Pattern.compile("^[^ ]{6,15}$");

		Matcher m = p.matcher(password);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}

	class RegisterListener extends KeyAdapter implements ActionListener {

		public void getData() {
			if (regView.addressField.getText().trim().equals("")
					|| regView.portField.getText().trim().equals("")
					|| regView.nickNameField.getText().trim().equals("")) {
				regView.displayErrorMessage("Not all fields are filled");
				return;
			}
			try {
				address = regView.getAddress();
				port = regView.getPort();
				nick = regView.getNickName();
				password = String.valueOf(regView.getPass());
				if (!checkPasswordValidity(password)) {
					regView.displayErrorMessage("Incorrect password");
					regView.passwordField.setText("");
					regView.setVisible(true);
					return;
				}
				if (!ClientModel.connect(address, port)) {
					regView.displayErrorMessage("Coudn't connect to the server.\nCheck address and port.");
					regView.addressField.setText("");
					regView.portField.setText("");
					regView.setVisible(true);
				} else {
					registrationRequest();
				}
			} catch (NumberFormatException ex) {
				regView.displayErrorMessage("Error! Port must be a number");
			}
		}

		public void actionPerformed(ActionEvent e) {
			getData();
		}

		public void keyPressed(KeyEvent e) {
			if (e.getKeyChar() == KeyEvent.VK_ENTER) {
				getData();
			}
		}

		public void registrationRequest() {
			try {
				if ((Boolean) ClientModel.userRegistration(nick, password) == false) {
					regView.displayErrorMessage("User with that name already exists. Choose another name");
				} else {
					regView.displayInfoMessage("Registration was successful!");
					regView.addressField.setText("");
					regView.portField.setText("");
					regView.nickNameField.setText("");
					regView.passwordField.setText("");
					regView.setVisible(false);
				}
			} catch (Exception e) {
				regView.displayErrorMessage("Error initialising chat: "
						+ e.getMessage());
			}
		}
	}

	class ExitListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			regView.setVisible(false);
		}
	}
}
