package Client;

public class Client {

	public static void main(String[] args) {

		LoginView loginView = new LoginView();
		ChatView chatView = new ChatView();
		RegisterView regView = new RegisterView();
		new LoginController(loginView, chatView, regView);
		new RegisterController(regView);
		new ChatController(chatView, loginView);

		loginView.setVisible(true);
	}
}