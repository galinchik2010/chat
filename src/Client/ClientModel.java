package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import Json.AbstractChatEntity;
import Json.ChatCommandsEnum;
import Json.ChatEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Halyna Mudryk
 * 
 *         Class that implements the connection to the server, sending a message
 *         to the server and receiving messages from the server
 * 
 */
public class ClientModel {

	private static Socket clientSocket = null;
	private static BufferedReader in = null;
	private static PrintWriter out = null;

	/**
	 * Try to connect to the server at the specified address and port
	 * 
	 * @param address
	 * @param port
	 * @return true if connect was successful, else false
	 */
	public static boolean connect(String address, int port) {
		try {
			clientSocket = new Socket(address, port);
			in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Check or the name unique, sending to the server json message and receive
	 * the answer
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static boolean userRegistration(String name, String password)
			throws Exception {
		String jsonString = null;

		AbstractChatEntity absChat = new AbstractChatEntity();
		absChat.setCommand(ChatCommandsEnum.REGISTRATION_REQUEST);
		ChatEntity registEntity = new ChatEntity();

		registEntity.setUsername(name);
		registEntity.setPassword(password);

		absChat.setObject(registEntity);

		ObjectMapper mapper = new ObjectMapper();
		jsonString = mapper.writeValueAsString(absChat);
		out.println(jsonString);
		jsonString = in.readLine();
		absChat = mapper.readValue(jsonString, AbstractChatEntity.class);

		return (Boolean) absChat.getObject();
	}

	public static int authorizeUser(String name, String password)
			throws Exception {
		String jsonString = null;

		AbstractChatEntity absChat = new AbstractChatEntity();
		absChat.setCommand(ChatCommandsEnum.AUTHORIZE_REQUEST);
		ChatEntity messEntity = new ChatEntity();
		messEntity.setUsername(name);
		messEntity.setPassword(password);

		absChat.setObject(messEntity);
		ObjectMapper mapper = new ObjectMapper();
		jsonString = mapper.writeValueAsString(absChat);
		out.println(jsonString);

		jsonString = in.readLine();

		absChat = mapper.readValue(jsonString, AbstractChatEntity.class);

		return (int) absChat.getObject();
	}

	/**
	 * Send the client message to the server in the form of jsonString
	 * 
	 * @param nick
	 * @param message
	 * @throws Exception
	 */
	public static void sendMessage(String time, String nick, String message)
			throws Exception {

		String jsonString = null;

		AbstractChatEntity absChat = new AbstractChatEntity();
		absChat.setCommand(ChatCommandsEnum.CHAT_MESSAGE);

		ChatEntity messEntity = new ChatEntity();
		messEntity.setTime(time);
		messEntity.setUsername(nick);
		messEntity.setMessage(message);

		absChat.setObject(messEntity);

		ObjectMapper mapper = new ObjectMapper();

		jsonString = mapper.writeValueAsString(absChat);

		out.println(jsonString);
	}

	/**
	 * Get the message in the form of jsonString from the server
	 * 
	 * @return
	 * @throws IOException
	 */
	public static AbstractChatEntity getAbstractServerMessage()
			throws IOException {
		String jsonString = null;
		ObjectMapper mapper = new ObjectMapper();

		jsonString = in.readLine();

		if (jsonString != null) {
			return mapper.readValue(jsonString, AbstractChatEntity.class);
		}

		return null;
	}

	/**
	 * Parsing the message from json string
	 * 
	 * @param absChat
	 * @return
	 * @throws IOException
	 */
	public static ChatEntity getChatMessageEntity(AbstractChatEntity absChat)
			throws IOException {
		ObjectMapper mapper = new ObjectMapper();

		String jsonString = mapper.writeValueAsString(absChat);

		JsonNode objectNode = mapper.readTree(jsonString).get("object");
		ChatEntity chatMessageEntity = mapper.readValue(objectNode.toString(),
				ChatEntity.class);

		return chatMessageEntity;
	}
}
