package Config;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

public class Config {
	// create server properties value
	private static String serverPort;
	private static String serverBacklog;
	private static String serverIp;

	// create database properties value
	private static String dbUrl;
	private static String dbName;
	private static String dbUsername;
	private static String dbPassword;
	
	private static Scanner scan;

	public static void main(String[] args) {

		Properties prop = new Properties();
		scan = new Scanner(System.in);

		System.out.println("Enter server port from 1024 to 65535: ");
		int numb = 0;
		do {

			while (!scan.hasNextInt()) {
				System.out.println("That's not a number! Try again");
				scan.next();
			}
			numb = scan.nextInt();
			if (numb < 1024 || numb > 65535) {
				System.err.println("Wrong port value! Try again");
			}
		} while (numb < 1024 || numb > 65535);

		serverPort = String.valueOf(numb);

		System.out.println("Enter server listen backlog: ");
		while (!scan.hasNextInt()) {
			System.out.println("That's not a number! Try again");
			scan.next();
		}
		numb = scan.nextInt();
		serverBacklog = String.valueOf(numb);

		System.out.println("Enter server ip address: ");
		serverIp = scan.next();

		System.out.println("Enter MySQL server url: ");
		dbUrl = scan.next();
		
		System.out.println("Enter MySQL server database name: ");
		dbName = scan.next();

		System.out.println("Enter MySQL server database username: ");
		dbUsername = scan.next();

		System.out.println("Enter MySQL server database password: ");
		dbPassword = scan.next();

		try {
			// set the properties value
			prop.setProperty("serverPort", serverPort);
			prop.setProperty("serverBackLog", serverBacklog);
			prop.setProperty("serverIp", serverIp);
			prop.setProperty("dbUrl", dbUrl);
			prop.setProperty("dbName", dbName);
			prop.setProperty("dbUsername", dbUsername);
			prop.setProperty("dbPassword", dbPassword);

			// save properties to project root folder
			prop.store(new FileOutputStream("config/config.properties"), null);
			System.out.println("Property file was created successfully!");
			return;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
