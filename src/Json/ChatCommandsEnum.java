package Json;

/**
 * Create a constants of commands
 * 
 * @author Sisters
 * 
 */
public interface ChatCommandsEnum {
	public static int REGISTRATION_REQUEST = 1;
	public static int REGISTRATION_RESPONSE = 2;
	public static int AUTHORIZE_REQUEST = 3;
	public static int AUTHORIZE_RESPONSE = 4;
	public static int AUTHORIZE_TRUE = 5;
	public static int AUTHORIZE_FALSE = 6;
	public static int AUTHORIZE_SAME_NAME = 7;
	public static int MEMBERS_LIST_RESPONSE = 8;
	public static int CHAT_MESSAGE = 9;
	public static int INFO_MESSAGE = 10;
}
