package Json;

/**
 * 
 * @author Sisters
 * 
 */
public class AbstractChatEntity {

	private int command;

	private Object object;

	public int getCommand() {
		return command;
	}

	public void setCommand(int command) {
		this.command = command;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

}
