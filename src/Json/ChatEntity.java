package Json;

/**
 * Create entity for getting message parameters from json string
 * 
 * @author Halyna Mudryk
 * 
 */
public class ChatEntity {

	private String time;
	private String username;
	private String password;
	private String message;

	public ChatEntity() {
	}

	public ChatEntity(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public ChatEntity(String time, String username, String message) {
		this.time = time;
		this.username = username;
		this.password = message;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
